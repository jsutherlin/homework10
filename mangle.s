@mangle
@Input: R0 contains number to mangle
@Output: R0 contains mangled number


	.global mangle

mangle:

	STMFD SP!, {R4, R5, LR}	@ Save R4 and R5
	
@Save integer for multiplication

	MOV R1, #8
	
@ Add 7

	ADD R0, R0, #7

@ Multiply by 8

	MUL R0, R0, R1

@ Subtract 15

	SUB R0, R0, #15
	
_exit:

	LDMFD SP!, {R4, R5, PC} @ Restore R4 and R5 
				@ POP the Link Register from the stack to get back
