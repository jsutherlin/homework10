
@divide_by_4
@Input: R0 contains number to be divided
@Output: R0 contains the quotient
@Output: R1 contains the remainder


.global divide_by_4

divide_by_4:

	STMFD SP!, {R4, R5, LR}	@ Save R4 and R5
	
@Set integer and counter

	MOV R2, #4
	MOV R3, #0

@Find how many times four goes into the number

_loop:

@do
	
	CMP R0, R2
	BLT _remainder
	
@while (R0 > R2)

	SUB R0, R0, R2
	ADD R3, R3, #1
	BAL _loop

@Take left over and save into R1
@Save counter into R0

_remainder:

	MOV R1, R0
	MOV R0, R3
	
_exit:
	LDMFD SP!, {R4, R5, PC} @ Restore R4 and R5 
				@ POP the Link Register from the stack to get back
