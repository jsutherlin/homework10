	.global main
	
main:

@Display your first and last name

	MOV R7, #4
	MOV R0, #1
	MOV R2, #16
	LDR R1, =string1
	SWI 0
	
@Prompt the user to enter a number or q

_ask:

	MOV R7, #4
	MOV R0, #1
	MOV R2, #39
	LDR R1, =string2
	SWI 0
	
_read:

	MOV R7, #3
	MOV R0, #0
	MOV R2, #11
	LDR R1, =input
	SWI 0
	
@Continue until you see a letter q to quit

	MOV R9, #0
	LDR R0, =input
	
_verify:

@do

	LDRB R1, [R0, R9]
	ADD R9, #1
	
@if (R1 == 113)

	CMP R1, #113
	BEQ _exit
	
@if (R1 == 10)

	CMP R1, #10
	BEQ _next1
	
@if (R1 < R2)

	CMP R1, #48
	BLT _nope
	
@if (R1 > 57)

	CMP R1, #57
	BGT _nope
	
@while (R1 > 47 && R1 < 58)
	
	BAL _verify
	
@If the user does not enter a number display an error message

_nope:

	MOV R7, #4
	MOV R0, #1
	MOV R2, #28
	LDR R1, =string3
	SWI 0
	BAL _ask
	
@Convert the Ascii digits to a numeric value using the to_binary function

_next1:

	BL to_binary
	
@Save result to R7 for mangle function

	MOV R7, R0

@Divide the number by 4 using the divide_by_4 function

	BL divide_by_4
	
@Move results to preserve them

	MOV R5, R0
	MOV R6, R1

@Display the result using printf
	
	LDR R0, =quotient
	BL printf
	
	MOV R1, R5
	LDR R0, =decimal_spec
	BL printf
	
	LDR R0, =remainder
	BL printf
	
	MOV R1, R6
	LDR R0, =decimal_spec
	BL printf
	
	LDR R0, =new_line
	BL printf
	
@Mangle the number using the mangle function

	MOV R0, R7
	BL mangle
	
@Move R0 to preserve number

	MOV R5, R0
	
@Display the result using printf
	
	LDR R0, =mangled
	BL printf
	
	MOV R1, R5
	LDR R0, =decimal_spec
	BL printf
	
	LDR R0, =new_line
	BL printf
	
	BAL _ask

_exit:
	
	MOV R7, #1
	SWI 0

.data
string1:
.ascii "James Sutherlin\n"
string2:
.ascii "Please enter a number or the letter q:\n"
string3:
.ascii "You did not enter a number.\n"
quotient:
.asciz "Quotient: "
remainder:
.asciz "Remainder: "
mangled:
.asciz "Mangled: "
decimal_spec:
.asciz "%d "
string_spec:
.asciz "%s "
input:
.ascii "          \n"
new_line:
.asciz "\n"
